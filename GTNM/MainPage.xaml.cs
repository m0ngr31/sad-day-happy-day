﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Phone.Tasks;

namespace GTNM
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;
            this.Loaded += new RoutedEventHandler(MainPage_Loaded);
        }

        // Load data for the ViewModel Items
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
        }

        public class feedsFormat
        {
            public string Title { get; set; }
            public List<string> Data { get; set; }
        }

        public class blankList : List<feedsFormat>
        {
            public blankList()
            {
                List<string> blank = new List<string>();
                new feedsFormat { Title = "", Data = blank };
            }
        }

        /*WebClient wc1;*/
        List<feedsFormat> fmlFeed = new List<feedsFormat>();

        private void pivotMain_LoadingPivotItem(object sender, PivotItemEventArgs e)
        {
            if (e.Item == fml)
            {

                fmlLoading.Visibility = System.Windows.Visibility.Visible;
                progressBar.IsIndeterminate = true;
                progressBar.Visibility = System.Windows.Visibility.Visible;
                listBox.ItemsSource = new blankList();
                WebClient wc = new WebClient();
                wc.OpenReadCompleted -= wc_OpenReadCompletedFML;
                wc.OpenReadCompleted += wc_OpenReadCompletedFML;
                wc.OpenReadAsync(new Uri("http://feeds.feedburner.com/feedburner/dHtY"));
            }

            if (e.Item == tfln)
            {
                if (App.IsTrial == true)
                {
                    tflnTrial.Visibility = System.Windows.Visibility.Visible;
                    tflnLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar2.IsIndeterminate = false;
                    progressBar2.Visibility = System.Windows.Visibility.Collapsed;
                }

                else
                {
                    tflnTrial.Visibility = System.Windows.Visibility.Collapsed;
                    tflnLoading.Visibility = System.Windows.Visibility.Visible;
                    progressBar2.IsIndeterminate = true;
                    progressBar2.Visibility = System.Windows.Visibility.Visible;
                    listBox2.ItemsSource = new blankList();
                    WebClient wc2 = new WebClient();
                    wc2.OpenReadCompleted -= wc_OpenReadCompletedTFLN;
                    wc2.OpenReadCompleted += wc_OpenReadCompletedTFLN;
                    wc2.OpenReadAsync(new Uri("http://feeds.feedburner.com/tfln"));
                }
            }

            if (e.Item == mlia)
            {
                if (App.IsTrial == true)
                {
                    mliaTrial.Visibility = System.Windows.Visibility.Visible;
                    mliaLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar3.IsIndeterminate = false;
                    progressBar3.Visibility = System.Windows.Visibility.Collapsed;
                }

                else
                {
                    mliaTrial.Visibility = System.Windows.Visibility.Collapsed;
                    mliaLoading.Visibility = System.Windows.Visibility.Visible;
                    progressBar3.IsIndeterminate = true;
                    progressBar3.Visibility = System.Windows.Visibility.Visible;
                    listBox3.ItemsSource = new blankList();
                    WebClient wc3 = new WebClient();
                    wc3.OpenReadCompleted -= wc_OpenReadCompletedMLIA;
                    wc3.OpenReadCompleted += wc_OpenReadCompletedMLIA;
                    wc3.OpenReadAsync(new Uri("http://feeds.feedburner.com/mlia"));
                }
            }

            if (e.Item == mdt)
            {
                if (App.IsTrial == true)
                {
                    mdtTrial.Visibility = System.Windows.Visibility.Visible;
                    mdtLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar4.IsIndeterminate = false;
                    progressBar4.Visibility = System.Windows.Visibility.Collapsed;
                }

                else
                {
                    mdtTrial.Visibility = System.Windows.Visibility.Collapsed;
                    mdtLoading.Visibility = System.Windows.Visibility.Visible;
                    progressBar4.IsIndeterminate = true;
                    progressBar4.Visibility = System.Windows.Visibility.Visible;
                    listBox4.ItemsSource = new blankList();
                    WebClient wc4 = new WebClient();
                    wc4.OpenReadCompleted -= wc_OpenReadCompletedMDT;
                    wc4.OpenReadCompleted += wc_OpenReadCompletedMDT;
                    wc4.OpenReadAsync(new Uri("http://mydrunktexts.com/rss"));
                }
            }

            if (e.Item == smds)
            {
                if (App.IsTrial == true)
                {
                    smdsTrial.Visibility = System.Windows.Visibility.Visible;
                    smdsLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar5.IsIndeterminate = false;
                    progressBar5.Visibility = System.Windows.Visibility.Collapsed;
                }

                else
                {
                    smdsTrial.Visibility = System.Windows.Visibility.Collapsed;
                    smdsLoading.Visibility = System.Windows.Visibility.Visible;
                    progressBar5.IsIndeterminate = true;
                    progressBar5.Visibility = System.Windows.Visibility.Visible;
                    listBox5.ItemsSource = new blankList();
                    WebClient wc5 = new WebClient();
                    wc5.OpenReadCompleted -= wc_OpenReadCompletedSMDS;
                    wc5.OpenReadCompleted += wc_OpenReadCompletedSMDS;
                    wc5.OpenReadAsync(new Uri("http://api.twitter.com/1/statuses/user_timeline.rss?screen_name=shitmydadsays"));
                }
            }

            if (e.Item == oito)
            {
                if (App.IsTrial == true)
                {
                    oitoTrial.Visibility = System.Windows.Visibility.Visible;
                    oitoLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar6.IsIndeterminate = false;
                    progressBar6.Visibility = System.Windows.Visibility.Collapsed;
                }

                else
                {
                    oitoTrial.Visibility = System.Windows.Visibility.Collapsed;
                    oitoLoading.Visibility = System.Windows.Visibility.Visible;
                    progressBar6.IsIndeterminate = true;
                    progressBar6.Visibility = System.Windows.Visibility.Visible;
                    listBox6.ItemsSource = new blankList();
                    WebClient wc6 = new WebClient();
                    wc6.OpenReadCompleted -= wc_OpenReadCompletedOITO;
                    wc6.OpenReadCompleted += wc_OpenReadCompletedOITO;
                    wc6.OpenReadAsync(new Uri("http://feeds.feedburner.com/overheardintheoffice/ffZs"));
                }
            }

            if (e.Item == oiny)
            {
                if (App.IsTrial == true)
                {
                    oinyTrial.Visibility = System.Windows.Visibility.Visible;
                    oinyLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar7.IsIndeterminate = false;
                    progressBar7.Visibility = System.Windows.Visibility.Collapsed;
                }

                else
                {
                    oinyTrial.Visibility = System.Windows.Visibility.Collapsed;
                    oinyLoading.Visibility = System.Windows.Visibility.Visible;
                    progressBar7.IsIndeterminate = true;
                    progressBar7.Visibility = System.Windows.Visibility.Visible;
                    listBox7.ItemsSource = new blankList();
                    WebClient wc7 = new WebClient();
                    wc7.OpenReadCompleted -= wc_OpenReadCompletedOINY;
                    wc7.OpenReadCompleted += wc_OpenReadCompletedOINY;
                    wc7.OpenReadAsync(new Uri("http://www.overheardinnewyork.com/feed"));
                }
            }

            if (e.Item == oatb)
            {
                if (App.IsTrial == true)
                {
                    oatbTrial.Visibility = System.Windows.Visibility.Visible;
                    oatbLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar8.IsIndeterminate = false;
                    progressBar8.Visibility = System.Windows.Visibility.Collapsed;
                }

                else
                {
                    oatbTrial.Visibility = System.Windows.Visibility.Collapsed;
                    oatbLoading.Visibility = System.Windows.Visibility.Visible;
                    progressBar8.IsIndeterminate = true;
                    progressBar8.Visibility = System.Windows.Visibility.Visible;
                    listBox8.ItemsSource = new blankList();
                    WebClient wc8 = new WebClient();
                    wc8.OpenReadCompleted -= wc_OpenReadCompletedOATB;
                    wc8.OpenReadCompleted += wc_OpenReadCompletedOATB;
                    wc8.OpenReadAsync(new Uri("http://www.overheardatthebeach.com/feed"));
                }
            }

            if (e.Item == bash)
            {
                if (App.IsTrial == true)
                {
                    bashTrial.Visibility = System.Windows.Visibility.Visible;
                    bashLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar9.IsIndeterminate = false;
                    progressBar9.Visibility = System.Windows.Visibility.Collapsed;
                }

                else {
                    bashTrial.Visibility = System.Windows.Visibility.Collapsed;
                    bashLoading.Visibility = System.Windows.Visibility.Visible;
                    progressBar9.IsIndeterminate = true;
                    progressBar9.Visibility = System.Windows.Visibility.Visible;
                    listBox9.ItemsSource = new blankList();
                    WebClient wc9 = new WebClient();
                    wc9.OpenReadCompleted -= wc_OpenReadCompletedBASH;
                    wc9.OpenReadCompleted += wc_OpenReadCompletedBASH;
                    wc9.OpenReadAsync(new Uri("http://feeds.feedburner.com/LatestBashorgQdbQuotes"));
                }
            }

            /*if (e.Item == nar)
            {
                narLoading.Visibility = System.Windows.Visibility.Visible;
                progressBar10.IsIndeterminate = true;
                progressBar10.Visibility = System.Windows.Visibility.Visible;
                listBox10.ItemsSource = new blankList();
                WebClient wc10 = new WebClient();
                wc10.OpenReadCompleted -= wc_OpenReadCompletedNAR;
                wc10.OpenReadCompleted += wc_OpenReadCompletedNAR;
                wc10.OpenReadAsync(new Uri("http://feeds.feedburner.com/FunnyStupidCustomerStories-NotAlwaysRight"));
            }*/

            /*if (e.Item == msp)
            {
                if (App.IsTrial == true)
                {
                    mspTrial.Visibility = System.Windows.Visibility.Visible;
                    mspLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar11.IsIndeterminate = false;
                    progressBar11.Visibility = System.Windows.Visibility.Collapsed;
                }
                else {
                    mspTrial.Visibility = System.Windows.Visibility.Collapsed;
                    mspLoading.Visibility = System.Windows.Visibility.Visible;
                    progressBar11.IsIndeterminate = true;
                    progressBar11.Visibility = System.Windows.Visibility.Visible;
                    listBox11.ItemsSource = new blankList();
                    WebClient wc11 = new WebClient();
                    wc11.OpenReadCompleted -= wc_OpenReadCompletedMSP;
                    wc11.OpenReadCompleted += wc_OpenReadCompletedMSP;
                    wc11.OpenReadAsync(new Uri("http://www.mysecretpost.com/rss.php"));
                }
            }*/

            if (e.Item == th)
            {
                if (App.IsTrial == true)
                {
                    thTrial.Visibility = System.Windows.Visibility.Visible;
                    thLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar12.IsIndeterminate = false;
                    progressBar12.Visibility = System.Windows.Visibility.Collapsed;
                }
                else
                {
                    thTrial.Visibility = System.Windows.Visibility.Collapsed;
                    thLoading.Visibility = System.Windows.Visibility.Visible;
                    progressBar12.IsIndeterminate = true;
                    progressBar12.Visibility = System.Windows.Visibility.Visible;
                    listBox12.ItemsSource = new blankList();
                    WebClient wc12 = new WebClient();
                    wc12.OpenReadCompleted -= wc_OpenReadCompletedTH;
                    wc12.OpenReadCompleted += wc_OpenReadCompletedTH;
                    wc12.OpenReadAsync(new Uri("http://feeds.feedburner.com/ThatHigh"));
                }
            }
        }

        private void buyMe(object sender, RoutedEventArgs e)
        {
            MarketplaceDetailTask marketplaceDetailTask = new MarketplaceDetailTask();
            marketplaceDetailTask.ContentType = MarketplaceContentType.Applications;
            marketplaceDetailTask.ContentIdentifier = "c2f02495-171a-42fc-963b-f1c7ff6d7d64";
            marketplaceDetailTask.Show();
        }

        private void wc_OpenReadCompletedTFLN(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                return;
            }

            using (Stream s = e.Result)
            {
                try {

                    XDocument feedXML = XDocument.Load(s);
                    var feeds = from feed in feedXML.Descendants("item")
                        select new
                        {
                            Title = feed.Element("title"),
                            Date = feed.Element("pubDate"),
                            Description = feed.Element("description")
                        };

                    tflnTrial.Visibility = System.Windows.Visibility.Collapsed;
                    tflnLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar2.IsIndeterminate = false;
                    progressBar2.Visibility = System.Windows.Visibility.Collapsed;

                    //List<feedsFormat> fmlFeed = new List<feedsFormat>();
                    fmlFeed.Clear();

                    foreach (var feed in feeds)
                    {
                        string content = feed.Description.Value;
                        content = StripTagsCharArray(content);
                        content = content.TrimEnd();
                        content = HttpUtility.HtmlDecode(content);

                        string title = content.Substring(0, 7);
                        string date = feed.Date.Value.Substring(5, 6);

                        content = content.Remove(0, 7);

                        List<string> tempWrapped = Wrap(content, 46);

                        /* listBox2.FontSize = 20;
                        listBox2.Items.Add(date + " - " + title);
                        tempWrapped.ForEach(i => listBox2.Items.Add(i));
                        listBox2.Items.Add("\n"); */

                        fmlFeed.Add(new feedsFormat { Title = title, Data = tempWrapped });

                    }

                    listBox2.ItemsSource = fmlFeed;

                }

                catch
                {
                    //System.Diagnostics.Debug.WriteLine("Well that didn't work dummy");
                    MessageBox.Show("The feed doesn't seem to be loading right now. Please try again later.");
                    tflnTrial.Visibility = System.Windows.Visibility.Collapsed;
                    tflnLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar2.IsIndeterminate = false;
                    progressBar2.Visibility = System.Windows.Visibility.Collapsed;
                }

                s.Close();
            }
        }

        private void wc_OpenReadCompletedFML(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                return;
            }

            using (Stream s = e.Result)
            {
                try {

                    XDocument feedXML = XDocument.Load(s);

                    var feeds = from feed in feedXML.Descendants("item")
                        select new
                        {
                            Title = feed.Element("title"),
                            Date = feed.Element("pubDate"),
                            Description = feed.Element("description")
                        };

                    fmlLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar.IsIndeterminate = false;
                    progressBar.Visibility = System.Windows.Visibility.Collapsed;
                
                    fmlFeed.Clear();

                    foreach (var feed in feeds)
                    {
                        string content = feed.Description.Value;
                        content = StripTagsCharArray(content);
                        content = content.TrimEnd();
                        content = HttpUtility.HtmlDecode(content);

                        string title = Regex.Replace(feed.Title.Value, "\\p{P}+", "");
                        string FML = " FML";
                        int findFML = title.IndexOf(FML);
                        string newTitle = title.Substring(0, findFML);
                        string date = feed.Date.Value.Substring(5, 6);

                        List<string> tempWrapped = Wrap(content, 46);

                        /*listBox.FontSize = 30;
                        listBox.Items.Add(date + " - " + newTitle + ":");
                        listBox.FontSize = 20;
                        tempWrapped.ForEach(i => listBox.Items.Add(i));
                        listBox.Items.Add("\n");*/
                        fmlFeed.Add(new feedsFormat { Title = title, Data = tempWrapped });
                    }

                    listBox.ItemsSource = fmlFeed;

                }

                catch
                {
                    //System.Diagnostics.Debug.WriteLine("Well that didn't work dummy");
                    MessageBox.Show("The feed doesn't seem to be loading right now. Please try again later.");
                    fmlLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar.IsIndeterminate = false;
                    progressBar.Visibility = System.Windows.Visibility.Collapsed;
                }

                s.Close();
            }
        }

        private void wc_OpenReadCompletedMLIA(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                return;
            }

            using (Stream s = e.Result)
            {
                try {

                    XDocument feedXML = XDocument.Load(s);

                    var feeds = from feed in feedXML.Descendants("item")
                                select new
                                {
                                    Title = feed.Element("title"),
                                    Date = feed.Element("pubDate"),
                                    Description = feed.Element("description")
                                };

                    mliaTrial.Visibility = System.Windows.Visibility.Collapsed;
                    mliaLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar3.IsIndeterminate = false;
                    progressBar3.Visibility = System.Windows.Visibility.Collapsed;

                    fmlFeed.Clear();

                    foreach (var feed in feeds)
                    {
                        string content = feed.Description.Value;
                        content = StripTagsCharArray(content);
                        content = content.TrimEnd();
                        content = HttpUtility.HtmlDecode(content);
                    
                        string date = feed.Date.Value.Substring(5, 6);

                        List<string> tempWrapped = Wrap(content, 48);

                        /*listBox3.FontSize = 30;
                        listBox3.Items.Add(date);
                        listBox3.FontSize = 20;
                        tempWrapped.ForEach(i => listBox3.Items.Add(i));
                        listBox3.Items.Add("\n");*/

                        fmlFeed.Add(new feedsFormat { Data = tempWrapped });

                    }

                    listBox3.ItemsSource = fmlFeed;

                }

                catch
                {
                    //System.Diagnostics.Debug.WriteLine("Well that didn't work dummy");
                    MessageBox.Show("The feed doesn't seem to be loading right now. Please try again later.");
                    mliaTrial.Visibility = System.Windows.Visibility.Collapsed;
                    mliaLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar3.IsIndeterminate = false;
                    progressBar3.Visibility = System.Windows.Visibility.Collapsed;
                }

                s.Close();
            }
        }

        private void wc_OpenReadCompletedMDT(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                return;
            }

            using (Stream s = e.Result)
            {
                try {

                    XDocument feedXML = XDocument.Load(s);

                    var feeds = from feed in feedXML.Descendants("item")
                                select new
                                {
                                    Title = feed.Element("title"),
                                    Date = feed.Element("pubDate"),
                                    Description = feed.Element("description")
                                };

                    mdtTrial.Visibility = System.Windows.Visibility.Collapsed;
                    mdtLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar4.IsIndeterminate = false;
                    progressBar4.Visibility = System.Windows.Visibility.Collapsed;

                    fmlFeed.Clear();

                    foreach (var feed in feeds)
                    {
                        string content = feed.Description.Value;
                        content = StripTagsCharArray(content);
                        content = content.TrimEnd();
                        content = HttpUtility.HtmlDecode(content);

                        string title = feed.Title.Value;
                        int find9 = title.IndexOf("(");
                        string newTitle = title.Substring(find9, 5);

                        string date = feed.Date.Value.Substring(5, 6);

                        List<string> tempWrapped = Wrap(content, 48);

                        /*listBox4.FontSize = 30;
                        listBox4.Items.Add(date + " - " + newTitle + ":");
                        listBox4.FontSize = 20;
                        tempWrapped.ForEach(i => listBox4.Items.Add(i));
                        listBox4.Items.Add("\n");*/

                        fmlFeed.Add(new feedsFormat { Title = title, Data = tempWrapped });

                    }

                    listBox4.ItemsSource = fmlFeed;

                }
                
                catch
                {
                    //System.Diagnostics.Debug.WriteLine("Well that didn't work dummy");
                    MessageBox.Show("The feed doesn't seem to be loading right now. Please try again later.");
                    mdtTrial.Visibility = System.Windows.Visibility.Collapsed;
                    mdtLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar4.IsIndeterminate = false;
                    progressBar4.Visibility = System.Windows.Visibility.Collapsed;
                }

                s.Close();
            }
        }

        private void wc_OpenReadCompletedSMDS(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                return;
            }

            using (Stream s = e.Result)
            {
                try {

                    XDocument feedXML = XDocument.Load(s);

                    var feeds = from feed in feedXML.Descendants("item")
                                select new
                                {
                                    Title = feed.Element("title"),
                                    Date = feed.Element("pubDate"),
                                    Description = feed.Element("description")
                                };

                    smdsTrial.Visibility = System.Windows.Visibility.Collapsed;
                    smdsLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar5.IsIndeterminate = false;
                    progressBar5.Visibility = System.Windows.Visibility.Collapsed;

                    fmlFeed.Clear();

                    foreach (var feed in feeds)
                    {
                        string content = feed.Description.Value;
                        content = StripTagsCharArray(content);
                        content = content.TrimEnd();
                        content = HttpUtility.HtmlDecode(content);
                        content = content.Substring(15);

                        string date = feed.Date.Value.Substring(5, 6);

                        List<string> tempWrapped = Wrap(content, 48);

                        /*listBox5.FontSize = 30;
                        listBox5.Items.Add(date);
                        listBox5.FontSize = 20;
                        tempWrapped.ForEach(i => listBox5.Items.Add(i));
                        listBox5.Items.Add("\n");*/

                        fmlFeed.Add(new feedsFormat { Data = tempWrapped });

                    }

                    listBox5.ItemsSource = fmlFeed;

                }

                catch
                {
                    //System.Diagnostics.Debug.WriteLine("Well that didn't work dummy");
                    MessageBox.Show("The feed doesn't seem to be loading right now. Please try again later.");
                    smdsTrial.Visibility = System.Windows.Visibility.Collapsed;
                    smdsLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar5.IsIndeterminate = false;
                    progressBar5.Visibility = System.Windows.Visibility.Collapsed;
                }

                s.Close();
            }
        }

        private void wc_OpenReadCompletedOITO(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                return;
            }

            using (Stream s = e.Result)
            {
                try {

                    XDocument feedXML = XDocument.Load(s);
                    var feeds = from feed in feedXML.Descendants("item")
                                select new
                                {
                                    Title = feed.Element("title"),
                                    //Date = feed.Element("pubDate"),
                                    Description = feed.Element("description")
                                };

                    oitoTrial.Visibility = System.Windows.Visibility.Collapsed;
                    oitoLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar6.IsIndeterminate = false;
                    progressBar6.Visibility = System.Windows.Visibility.Collapsed;

                    //List<feedsFormat> fmlFeed = new List<feedsFormat>();
                    fmlFeed.Clear();

                    foreach (var feed in feeds)
                    {
                        string content = feed.Description.Value;
                        content = StripTagsCharArray(content);
                        content = content.TrimEnd();
                        content = HttpUtility.HtmlDecode(content);

                        string title = feed.Title.Value;
                        //string date = feed.Date.Value.Substring(5, 6);

                        //content = content.Remove(0, 7);

                        List<string> tempWrapped = Wrap(content, 46);

                        /* listBox2.FontSize = 20;
                        listBox2.Items.Add(date + " - " + title);
                        tempWrapped.ForEach(i => listBox2.Items.Add(i));
                        listBox2.Items.Add("\n"); */

                        fmlFeed.Add(new feedsFormat { Title = title, Data = tempWrapped });

                    }

                    listBox6.ItemsSource = fmlFeed;

                }

                catch
                {
                    //System.Diagnostics.Debug.WriteLine("Well that didn't work dummy");
                    MessageBox.Show("The feed doesn't seem to be loading right now. Please try again later.");
                    oitoTrial.Visibility = System.Windows.Visibility.Collapsed;
                    oitoLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar6.IsIndeterminate = false;
                    progressBar6.Visibility = System.Windows.Visibility.Collapsed;
                }

                s.Close();
            }
        }

        private void wc_OpenReadCompletedOINY(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                return;
            }

            using (Stream s = e.Result)
            {
                try {

                    XDocument feedXML = XDocument.Load(s);
                    var feeds = from feed in feedXML.Descendants("item")
                                select new
                                {
                                    Title = feed.Element("title"),
                                    //Date = feed.Element("pubDate"),
                                    Description = feed.Element("description")
                                };

                    oinyTrial.Visibility = System.Windows.Visibility.Collapsed;
                    oinyLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar7.IsIndeterminate = false;
                    progressBar7.Visibility = System.Windows.Visibility.Collapsed;

                    //List<feedsFormat> fmlFeed = new List<feedsFormat>();
                    fmlFeed.Clear();

                    foreach (var feed in feeds)
                    {
                        string content = feed.Description.Value;
                        content = StripTagsCharArray(content);
                        content = content.TrimEnd();
                        content = HttpUtility.HtmlDecode(content);

                        string title = feed.Title.Value;
                        //string date = feed.Date.Value.Substring(5, 6);

                        //content = content.Remove(0, 7);

                        List<string> tempWrapped = Wrap(content, 46);

                        /* listBox2.FontSize = 20;
                        listBox2.Items.Add(date + " - " + title);
                        tempWrapped.ForEach(i => listBox2.Items.Add(i));
                        listBox2.Items.Add("\n"); */

                        fmlFeed.Add(new feedsFormat { Title = title, Data = tempWrapped });

                    }

                    listBox7.ItemsSource = fmlFeed;

                }

                catch
                {
                    //System.Diagnostics.Debug.WriteLine("Well that didn't work dummy");
                    MessageBox.Show("The feed doesn't seem to be loading right now. Please try again later.");
                    oinyTrial.Visibility = System.Windows.Visibility.Collapsed;
                    oinyLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar7.IsIndeterminate = false;
                    progressBar7.Visibility = System.Windows.Visibility.Collapsed;
                }

                s.Close();
            }
        }

        private void wc_OpenReadCompletedOATB(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                return;
            }

            using (Stream s = e.Result)
            {
                try {

                    XDocument feedXML = XDocument.Load(s);
                    var feeds = from feed in feedXML.Descendants("item")
                                select new
                                {
                                    Title = feed.Element("title"),
                                    //Date = feed.Element("pubDate"),
                                    Description = feed.Element("description")
                                };

                    oatbTrial.Visibility = System.Windows.Visibility.Collapsed;
                    oatbLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar8.IsIndeterminate = false;
                    progressBar8.Visibility = System.Windows.Visibility.Collapsed;

                    //List<feedsFormat> fmlFeed = new List<feedsFormat>();
                    fmlFeed.Clear();

                    foreach (var feed in feeds)
                    {
                        string content = feed.Description.Value;
                        content = findBreak(content);
                        content = StripTagsCharArray(content);
                        content = content.TrimEnd();
                        content = HttpUtility.HtmlDecode(content);

                        string title = feed.Title.Value;
                        //string date = feed.Date.Value.Substring(5, 6);

                        var removeBreaks = new List<string>();
                        var removedBreaks = new List<string>();
                        var tempWrapped = new List<string>();
                        var finalWrapped = new List<string>();

                        removeBreaks = replaceBreak(content);
                        removedBreaks = removedBreaks.Concat(removeBreaks).ToList();

                        foreach (var item in removedBreaks)
                        {
                            //System.Diagnostics.Debug.WriteLine(item);
                            tempWrapped = Wrap(item, 48);
                            finalWrapped = finalWrapped.Concat(tempWrapped).ToList();
                        }

                        fmlFeed.Add(new feedsFormat { Title = title, Data = finalWrapped });

                    }

                    listBox8.ItemsSource = fmlFeed;

                }

                catch
                {
                    //System.Diagnostics.Debug.WriteLine("Well that didn't work dummy");
                    MessageBox.Show("The feed doesn't seem to be loading right now. Please try again later.");
                    oatbTrial.Visibility = System.Windows.Visibility.Collapsed;
                    oatbLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar8.IsIndeterminate = false;
                    progressBar8.Visibility = System.Windows.Visibility.Collapsed;
                }

                s.Close();
            }
        }

        private void wc_OpenReadCompletedBASH(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                return;
            }

            using (Stream s = e.Result)
            {
                try {

                    XDocument feedXML = XDocument.Load(s);
                    var feeds = from feed in feedXML.Descendants("item")
                                select new
                                {
                                    Title = feed.Element("title"),
                                    //Date = feed.Element("pubDate"),
                                    Description = feed.Element("description")
                                };

                    bashTrial.Visibility = System.Windows.Visibility.Collapsed;
                    bashLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar9.IsIndeterminate = false;
                    progressBar9.Visibility = System.Windows.Visibility.Collapsed;

                    //List<feedsFormat> fmlFeed = new List<feedsFormat>();
                    fmlFeed.Clear();

                    foreach (var feed in feeds)
                    {
                        string content = feed.Description.Value;
                        content = findBreak(content);
                        content = StripTagsCharArray(content);
                        content = content.TrimEnd();
                        content = HttpUtility.HtmlDecode(content);

                        string title = feed.Title.Value;

                        var removeBreaks = new List<string>();
                        var removedBreaks = new List<string>();
                        var tempWrapped = new List<string>();
                        var finalWrapped = new List<string>();

                        removeBreaks = replaceBreak(content);
                        removedBreaks = removedBreaks.Concat(removeBreaks).ToList();

                        foreach (var item in removedBreaks)
                        {
                            tempWrapped = Wrap(item, 48);
                            finalWrapped = finalWrapped.Concat(tempWrapped).ToList();
                        }

                        fmlFeed.Add(new feedsFormat { Title = title, Data = finalWrapped });

                    }

                    listBox9.ItemsSource = fmlFeed;

                }

                catch
                {
                    //System.Diagnostics.Debug.WriteLine("Well that didn't work dummy");
                    MessageBox.Show("The feed doesn't seem to be loading right now. Please try again later.");
                    bashTrial.Visibility = System.Windows.Visibility.Collapsed;
                    bashLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar9.IsIndeterminate = false;
                    progressBar9.Visibility = System.Windows.Visibility.Collapsed;
                }

                s.Close();
            }
        }

        /*private void wc_OpenReadCompletedNAR(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                return;
            }

            using (Stream s = e.Result)
            {
                try {

                    XDocument feedXML = XDocument.Load(s);
                    var feeds = from feed in feedXML.Descendants("item")
                                select new
                                {
                                    Title = feed.Element("title"),
                                    //Date = feed.Element("pubDate"),
                                    Description = feed.Element("description")
                                };

                    narLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar10.IsIndeterminate = false;
                    progressBar10.Visibility = System.Windows.Visibility.Collapsed;

                    //List<feedsFormat> fmlFeed = new List<feedsFormat>();
                    fmlFeed.Clear();

                    foreach (var feed in feeds)
                    {
                        string content = feed.Description.Value;
                        content = findBreak(content);
                        content = StripTagsCharArray(content);
                        content = content.TrimEnd();
                        content = HttpUtility.HtmlDecode(content);

                        string title = feed.Title.Value;

                        var removeBreaks = new List<string>();
                        var removedBreaks = new List<string>();
                        var tempWrapped = new List<string>();
                        var finalWrapped = new List<string>();

                        removeBreaks = replaceBreak(content);
                        removedBreaks = removedBreaks.Concat(removeBreaks).ToList();

                        foreach (var item in removedBreaks)
                        {
                            //System.Diagnostics.Debug.WriteLine(item);
                            tempWrapped = Wrap(item, 48);
                            finalWrapped = finalWrapped.Concat(tempWrapped).ToList();
                        }

                        fmlFeed.Add(new feedsFormat { Title = title, Data = finalWrapped });

                    }

                    listBox10.ItemsSource = fmlFeed;

                }

                catch
                {
                    //System.Diagnostics.Debug.WriteLine("Well that didn't work dummy");
                    MessageBox.Show("The feed doesn't seem to be loading right now. Please try again later.");
                    narLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar10.IsIndeterminate = false;
                    progressBar10.Visibility = System.Windows.Visibility.Collapsed;
                }

                s.Close();
            }
        }*/

        /*private void wc_OpenReadCompletedMSP(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                return;
            }

            using (Stream s = e.Result)
            {
                try
                {

                    XDocument feedXML = XDocument.Load(s);

                    var feeds = from feed in feedXML.Descendants("item")
                                select new
                                {
                                    Title = feed.Element("title"),
                                    Date = feed.Element("pubDate"),
                                    Description = feed.Element("description")
                                };

                    mspTrial.Visibility = System.Windows.Visibility.Collapsed;
                    mspLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar11.IsIndeterminate = false;
                    progressBar11.Visibility = System.Windows.Visibility.Collapsed;

                    fmlFeed.Clear();

                    foreach (var feed in feeds)
                    {
                        string content = feed.Description.Value;
                        content = StripTagsCharArray(content);
                        content = content.TrimEnd();
                        content = HttpUtility.HtmlDecode(content);
                        //content = content.Substring(15);
                        int index = content.IndexOf("By:");
                        string title = content.Substring(index);
                        int index1 = title.IndexOf("on ");
                        title = title.Substring(0, index1);
                        content = content.Substring(0, index);

                        //string date = feed.Date.Value.Substring(5, 6);

                        List<string> tempWrapped = Wrap(content, 48);

                        fmlFeed.Add(new feedsFormat { Title = title, Data = tempWrapped });

                    }

                    listBox11.ItemsSource = fmlFeed;
                }

                catch
                {
                    //System.Diagnostics.Debug.WriteLine("Well that didn't work dummy");
                    MessageBox.Show("The feed doesn't seem to be loading right now. Please try again later.");
                    mspTrial.Visibility = System.Windows.Visibility.Collapsed;
                    mspLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar11.IsIndeterminate = false;
                    progressBar11.Visibility = System.Windows.Visibility.Collapsed;
                }
                
                s.Close();
            }
        }*/

        private void wc_OpenReadCompletedTH(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                return;
            }

            using (Stream s = e.Result)
            {
                //System.Diagnostics.Debug.WriteLine(s.Read);
                //XDocument feedXML = XDocument.Load(s);
                try {
                    
                    XDocument feedXML = XDocument.Load(s); 
                
                    var feeds = from feed in feedXML.Descendants("item")
                                select new
                                {
                                    Title = feed.Element("title"),
                                    Date = feed.Element("pubDate"),
                                    Description = feed.Element("description")
                                };

                    thLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar12.IsIndeterminate = false;
                    progressBar12.Visibility = System.Windows.Visibility.Collapsed;
                    thTrial.Visibility = System.Windows.Visibility.Collapsed;

                    fmlFeed.Clear();

                    foreach (var feed in feeds)
                    {
                        string content = feed.Description.Value;
                        content = StripTagsCharArray(content);
                        content = content.TrimEnd();
                        content = HttpUtility.HtmlDecode(content);
                        content = content.Substring(15);

                        string date = feed.Date.Value.Substring(5, 6);

                        List<string> tempWrapped = Wrap(content, 48);

                        /*listBox5.FontSize = 30;
                        listBox5.Items.Add(date);
                        listBox5.FontSize = 20;
                        tempWrapped.ForEach(i => listBox5.Items.Add(i));
                        listBox5.Items.Add("\n");*/

                        fmlFeed.Add(new feedsFormat { Data = tempWrapped });

                    }

                    listBox12.ItemsSource = fmlFeed;

                }

                catch
                {
                    //System.Diagnostics.Debug.WriteLine("Well that didn't work dummy");
                    MessageBox.Show("The feed doesn't seem to be loading right now. Please try again later.");
                    thLoading.Visibility = System.Windows.Visibility.Collapsed;
                    progressBar12.IsIndeterminate = false;
                    progressBar12.Visibility = System.Windows.Visibility.Collapsed;
                    thTrial.Visibility = System.Windows.Visibility.Collapsed;

                    //System.Diagnostics.Debug.WriteLine(App.IsTrial);
                }

                s.Close();
            }
        }

        public static string findBreak(string original)
        {
            //
            //        REGEX Examples:
            //        @"(?:\<br[^>]*\>)*$"
            //        @"(<br */>)|(\[br */\])"
            //        @"<br\s*/>"
            //        "(?i)<br\\s*/>"
            //        "<[Bb][Rr][^>]*>"
            return Regex.Replace(original, "<[Bb][Rr][^>]*>", "sdhd123");
        }

        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

        static List<string> Wrap(string text, int margin)
        {
            int start = 0, end;
            var lines = new List<string>();
            text = Regex.Replace(text, @"\s", " ").Trim();

            while ((end = start + margin) < text.Length)
            {
                while (text[end] != ' ' && end > start)
                    end -= 1;

                if (end == start)
                    end = start + margin;

                lines.Add(text.Substring(start, end - start));
                start = end + 1;
            }

            if (start < text.Length)
                lines.Add(text.Substring(start));

            return lines;
        }

        static List<string> replaceBreak(string origList)
        {
            var modifiedList = new List<string>();

            if (origList.IndexOf("sdhd123") != -1)
            {
                modifiedList = Regex.Split(origList, "sdhd123").ToList();
                
            }
            else
            {
                modifiedList.Add(origList);
            }

            return modifiedList;
        }
    }
}